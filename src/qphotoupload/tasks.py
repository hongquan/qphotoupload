# To be run in child processes

import os
import time
from datetime import datetime
from http.server import HTTPServer, SimpleHTTPRequestHandler

from .env import HTTP_PORT
from .util import shrink_photo
from .srv import FacebookSession, GoogleSession

# Simple web server, to handle callback of OAuth Login
simple_server = HTTPServer(('localhost', HTTP_PORT),
                           SimpleHTTPRequestHandler)


def run_server():
    simple_server.serve_forever()


def get_facebook_albums(access_token, queue):
    fb = FacebookSession(access_token)
    albums = tuple(fb.get_albums())
    queue.put(albums)
    time.sleep(1)


def get_google_albums(access_token, userid, queue):
    gg = GoogleSession(access_token, userid)
    albums = tuple(gg.get_albums())
    queue.put(albums)
    time.sleep(1)


def add_facebook_album(name, access_token, queue):
    fb = FacebookSession(access_token)
    aid = fb.add_album(name)
    queue.put(aid)
    time.sleep(1)


def add_google_album(name, access_token, queue):
    gg = GoogleSession(access_token)
    aid = gg.add_album(name)
    queue.put(aid)
    time.sleep(1)


def delete_google_album(aid, access_token, queue):
    gg = GoogleSession(access_token)
    queue.put(gg.delete_album(aid))
    time.sleep(1)



def resize_in_child_process(size, thumb_map):
    for p, (ready, desc) in thumb_map.items():
        if '://' not in p:
            if not size:
                # Resize is not required and path is not a URI
                thumb_map[p] = (True, desc)
            else:
                thumb_map[p] = (shrink_photo(p, size), desc)


def upload_in_child_process(conn, thumb_map):
    service, access_token, album_id = conn.recv()
    if service == 'facebook':
        oclient = FacebookSession(access_token)
    elif service == 'google':
        oclient = GoogleSession(access_token)
    for i, path in enumerate(thumb_map.keys()):
        # Use an inner loop because we may want to wait
        # for photo being resized.
        for j in range(100):
            thumb, desc = thumb_map[path]
            if type(thumb) is str and os.path.exists(thumb):
                # Upload resized version
                start_time = datetime.now()
                pid = oclient.upload_photo(album_id, thumb, desc)
                duration = datetime.now() - start_time
                # Send tuple (new created photo ID, row index,
                # upload byte size, upload duration) to parent process
                conn.send((pid, i, os.stat(thumb).st_size, duration.seconds))
                # Remove temporary file
                os.unlink(thumb)
                break
            elif thumb is True or \
                 (type(thumb) is str and not os.path.exists(thumb)):
                # Upload origin
                start_time = datetime.now()
                pid = oclient.upload_photo(album_id, path, desc)
                duration = datetime.now() - start_time
                conn.send((pid, i, os.stat(path).st_size, duration.seconds))
                break
            # Photo resizing is not finish, wait
            time.sleep(0.05)

    time.sleep(2) # Wait for parent process collect enough data
    conn.send(('finish', None, None, None))
    time.sleep(2) # Wait for parent process collect enough data
    conn.close()
