import os
import logging
import queue
from collections import OrderedDict
from timeit import default_timer as timer
from multiprocessing import Process, Pipe, Queue

from gi.repository import Gio, Gtk, Gdk
from requests.exceptions import ConnectionError

from .env import __version__, APP_NAME, APP_REGISTER_ID,\
                 DEFAULT_WINDOW_SIZE, HTTP_PORT, SUPPORTTED_SERVICES
from .util import get_ui_file, PManager, create_local_thumbnail, create_local_file
from .ui import QPhotoUploadUi
from .acc import load_accounts, Account, remove_account
from .srv import FacebookService, FacebookSession,\
                 GoogleService, GoogleSession
from .tasks import get_facebook_albums, add_facebook_album,\
                   get_google_albums, add_google_album,\
                   delete_google_album, run_server, \
                   resize_in_child_process, upload_in_child_process


logger = logging.getLogger(__name__)

class QPhotoUploadApp(Gtk.Application):
    def __init__(self):
        super().__init__(application_id=APP_REGISTER_ID,
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.setup_ui()
        # Mandatory
        self.connect('activate', self.on_activated)
        self._gio_copies = {}
        self._child_processes = []
        self.assumed_speed = 15360 # 15kB/s
        # These 3 variables are for guess_current_file_size()
        self._thumb_generated = False
        self._current_filesize = 0
        self._last_index = 0


    def setup_ui(self):
        self.ui = QPhotoUploadUi(self)
        self.ui.connect_handlers([])


    def refresh_ui(self):
        while Gtk.events_pending():
            Gtk.main_iteration()


    def on_activated(self, userdata=None):
        window = self.ui.window
        title = '{} - {}'.format(APP_NAME, __version__)
        window.set_title(title)
        window.resize(*DEFAULT_WINDOW_SIZE)
        window.show_all()
        # Mandatory
        self.add_window(window)
        # Load accounts
        self.load_accounts()


    def quit(self, *args):
        [p.terminate() for p in self._child_processes]
        super().quit()


    def add_files(self, files):
        self.ui.add_files(files)


    def load_accounts(self):
        store = self.ui.store_accounts
        # Note to exclude "id" from account record
        [store.append(a[1:]) for a in load_accounts()]


    def run_server(self):
        self.server_process = p = Process(target=run_server)
        self._child_processes.append(p)
        p.start()


    def stop_server(self):
        self.server_process.terminate()


    def add_account(self, service):
        if service not in SUPPORTTED_SERVICES:
            return
        if service == 'facebook':
            acc = self.add_facebook_account()
        elif service == 'google':
            acc = self.add_google_account()
        if not acc:
            return
        acc.save()
        itr = self.ui.store_accounts.append(acc.as_tuple())
        self.ui.combo_account.set_active_iter(itr)


    def remove_account(self, service, uid):
        return remove_account(service, uid)


    def add_facebook_account(self):
        fb = FacebookService()
        r = self.ui.login_to_oauth(fb.authorize_url, fb.is_authorize_success_url)
        if not r:
            return
        authses = fb.get_auth_session()
        access_token = authses.access_token
        user = authses.get('me').json()
        return Account(service='facebook', access_token=access_token,
                       uid=user['id'], email=user.get('email'))


    def add_google_account(self):
        gg = GoogleService()
        r = self.ui.login_to_oauth(gg.authorize_url, gg.is_authorize_success_url)
        if not r:
            return
        authses = gg.get_auth_session()
        access_token = authses.access_token
        user = authses.get('https://www.googleapis.com/oauth2/v1/userinfo').json()
        return Account(service='google', access_token=access_token,
                       uid=user['id'], email=user['email'])


    def load_albums(self, service, access_token, userid=None):
        if service not in ('facebook', 'google'):
            return
        old_index = self.ui.combo_album.get_active()
        self.ui.store_albums.clear()
        q = Queue()
        if service == 'facebook':
            p = Process(target=get_facebook_albums, args=(access_token, q))
        else:
            p = Process(target=get_google_albums, args=(access_token, userid, q))
        p.start()
        albums = ()
        self.ui.wait_load_albums()
        while p.is_alive() and not albums:
            try:
                albums = q.get(True, 0.05)
            except queue.Empty:
                self.refresh_ui()
                continue
        self.ui.unwait_load_albums()
        for album in albums:
            self.ui.store_albums.append(album)
        if old_index != -1:
            self.ui.combo_album.set_active(old_index)


    def add_album(self, name, service, access_token):
        if service not in SUPPORTTED_SERVICES:
            return
        q = Queue()
        if service == 'facebook':
            p = Process(target=add_facebook_album, args=(name, access_token, q))
        else:
            p = Process(target=add_google_album, args=(name, access_token, q))
        p.start()
        aid = None
        self.ui.wait_load_albums()
        while p.is_alive() and aid is None:
        # If failed to add album, aid is "False", not "None"
            try:
                aid = q.get(True, 0.05)
            except queue.Empty:
                self.refresh_ui()
                continue
        self.ui.unwait_load_albums()
        if aid:
            self.load_albums(service, access_token)
            self.ui.make_album_selected(aid)


    def delete_album(self, aid, service, access_token):
        if service != 'google':
            return
        q = Queue()
        p = Process(target=delete_google_album, args=(aid, access_token, q))
        p.start()
        self.ui.wait_load_albums()
        while p.is_alive():
            try:
                success = q.get(True, 0.05)
            except queue.Empty:
                self.refresh_ui()
                continue
            if success:
                break
        self.ui.unwait_load_albums()
        self.load_albums(service, access_token)



    def upload(self, service, access_token, album_id):
        if service not in ('facebook', 'google'):
            return
        self._gio_copies = {}
        gio_paths = tuple(p for p, desc in self.ui.get_photo_paths() if '://' in p)
        # GFile of non-local
        for p in gio_paths:
            gfile = Gio.file_new_for_uri(p)
            gfile.load_contents_async(None, self.cb_load_content_gio,
                                      (service, access_token, album_id))
        if not gio_paths:
            self.multiprocess_upload(service, access_token, album_id)


    def multiprocess_upload(self, service, access_token, album_id):
        total = len(self.ui.store_thumbnails)
        count = 0
        size = self.ui.get_shrink_size()
        filedict = OrderedDict((p, [None, d]) for p, d in self.ui.get_photo_paths())
        # This is a dict
        # filepath: (thumbnail_path, description)

        # Update file dict with thumb of Gio files
        for p, t in self._gio_copies.items():
            if t:
                filedict[p][0] = t
            else:
                # This file could not be read successfully.
                del filedict[p]

        # Here we will use 2 child processes:
        # One for resizing (shrinking) photos (A)
        # and one for uploading photos (B).
        # Parent and B will communicate via a pipe,
        # to give access token, album ID, and to receive
        # new created photo ID.
        # Beside, we need to shared dictionary to store
        # the path of resized file, between A & B.
        # A will update the dict and B receives.

        with PManager() as manager:
            # Process to shrink photo
            thumb_map = manager.OrderedDict(filedict)
            p_resize = Process(target=resize_in_child_process,
                               args=(size, thumb_map))
            p_resize.start()

            # Process to upload
            parent_conn, child_conn = Pipe()
            p_upload = Process(target=upload_in_child_process,
                               args=(child_conn, thumb_map))
            # Send access token and album ID to child process
            parent_conn.send((service, access_token, album_id))
            p_upload.start()

            self.ui.reset_all_progress_bars()

            self._child_processes.append(p_resize)
            self._child_processes.append(p_upload)
            time_start = timer()
            while p_upload.is_alive() and count < total:
                child_sent = parent_conn.poll(0.05)
                eta = timer() - time_start  # Established time
                if not child_sent:
                    filesize = self.guess_current_file_size(thumb_map, count)
                    self.ui.set_progress(count,
                                         min(100*self.assumed_speed*eta/filesize, 98))
                    # Make UI updated
                    self.refresh_ui()
                    continue
                # Data from child process is tuple
                # (new-created-photo-ID, row-index-in-GtkListStore,
                # uploaded-bytes, uploaded-seconds)
                aid, row, upsize, uptime = parent_conn.recv()
                if aid == 'finish':
                    break
                if type(aid) is int:
                    self.ui.set_progress(count, 100)
                    count += 1
                    time_start = timer() # Reset timer
                    self.ui.set_progress(count-1, 100)
                    self.ui.bar_upload.set_fraction(count/total)
                    # Make UI updated
                    self.refresh_ui()
                    self.assumed_speed = upsize/uptime

        self._child_processes.remove(p_upload)
        self._child_processes.remove(p_resize)
        self.ui.btn_upload.set_sensitive(True)
        # Reload albums
        self.load_albums(service, access_token)


    def cb_load_content_gio(self, gfile, res, upload_setting):
        success, content, etag = gfile.load_contents_finish(res)
        if not success:
            self._gio_copies[gfile.get_ui()] = False
            return
        size = self.ui.get_shrink_size()
        if size:
            thumb = create_local_thumbnail(content, size)
        else:
            thumb = create_local_file(content)
        del content
        self._gio_copies[gfile.get_uri()] = thumb
        gio_paths = tuple(p for p, desc in self.ui.get_photo_paths() if '://' in p)
        if len(self._gio_copies) == len(gio_paths):
            self.multiprocess_upload(*upload_setting)


    def guess_current_file_size(self, thumb_map, count):
        if self._current_filesize \
           and count == self._last_index \
           and self._thumb_generated:
            return self._current_filesize

        if count != self._last_index: # Process to new file
            self._last_index = count
            self._thumb_generated = False
            self._current_filesize = 0

        if self._current_filesize:
            return self._current_filesize

        for i, p in enumerate(thumb_map.keys()):
            # Cannot access by index, so have to use enumerate
            if i != count:
                continue
            thumb = thumb_map[p][0]
            if type(thumb) is str and os.path.exists(thumb):
                self._current_filesize = os.stat(thumb).st_size
                self._thumb_generated = True
                return self._current_filesize
            else:   # Thumb not exist, get original file's size
                self._current_filesize = os.stat(p).st_size
                return self._current_filesize
