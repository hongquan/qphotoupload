import os
import io
import glob
import tempfile
from collections import OrderedDict
from multiprocessing.managers import BaseManager, DictProxy
from PIL import Image

from .env import get_data_dir, PACKAGE_NAME

def get_ui_file(name):
    return os.path.join(get_data_dir(), name+'.ui')


def get_app_icon_list():
    folder = os.path.join(get_data_dir(), 'icons')
    pattern = '{}*.png'.format(PACKAGE_NAME)
    return glob.iglob(os.path.join(folder, pattern))


def get_main_icon_file():
    folder = os.path.join(get_data_dir(), 'icons')
    return os.path.join(folder, PACKAGE_NAME+'.svg')


# Server process for multiprocessing
class PManager(BaseManager):
    pass

PManager.register('OrderedDict', OrderedDict, DictProxy)


def generate_temp_path():
    # Get a random temporary file path
    tm, tm_path = tempfile.mkstemp()
    os.close(tm)
    return tm_path


def shrink_photo(path, size):
    tm_path = generate_temp_path()
    with open(path, 'rb') as fl:
        out = create_thumbnail(fl, size, tm_path)
    return out


def create_thumbnail(fd, size, outpath, force=False):
    img = Image.open(fd)
    ow, oh = img.size
    if ow <= size and oh <= size and not force:
        # No need to resize
        return True
    img.thumbnail((size, size), Image.ANTIALIAS)
    img.save(outpath, img.format)
    return outpath


def create_local_file(content):
    # Get a random temporary file path
    tm_path = generate_temp_path()
    with open(tm_path, 'wb') as fl:
        fl.write(content)
    return tm_path


def create_local_thumbnail(content, size):
    # Get a random temporary file path
    tm_path = generate_temp_path()
    return create_thumbnail(io.BytesIO(content), size, tm_path, True)

