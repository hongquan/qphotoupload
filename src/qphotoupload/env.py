
import os
import json

__author__ = 'Nguyễn Hồng Quân <ng.hong.quan@gmail.com>'
__version_info__ = (1, 4)
__version__ = '.'.join(str(i) for i in __version_info__)

APP_NAME = 'QPhotoUpload'
PACKAGE_NAME = APP_NAME.lower()
APP_REGISTER_ID = 'vn.hoabinh.quan.' + PACKAGE_NAME
DEFAULT_WINDOW_SIZE = (560, 410)

SUPPORTTED_SERVICES = ('facebook', 'google')
HTTP_PORT = 7654

def get_data_dir():
    # Check if we are running from development source
    folder = os.path.dirname(os.path.abspath(__file__))
    prfolder = os.path.dirname(folder)
    pjfolder, prname = os.path.split(prfolder)
    if prname == 'src':
        # In development source
        return os.path.join(pjfolder, 'data')
    # else, we may run from installed location
    if pjfolder.startswith('/usr/local'):
        prefix = '/usr/local/share'
    elif pjfolder.startswith('/usr'):
        prefix = '/usr/share'
    else:
        prefix = '/opt'
    return os.path.join(prefix, PACKAGE_NAME)


def get_user_dir():
    home = os.path.expanduser('~')
    datadir = os.path.join(home, '.local', 'share', PACKAGE_NAME)
    if not os.path.exists(datadir):
        os.makedirs(datadir)
    return datadir
