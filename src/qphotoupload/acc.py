import os
import os.path
from peewee import Model, SqliteDatabase
from peewee import CharField

from .env import SUPPORTTED_SERVICES, get_user_dir

filepath = os.path.join(get_user_dir(), 'accounts.db')
db = SqliteDatabase(filepath)

class Account(Model):
    service = CharField(choices=SUPPORTTED_SERVICES)
    access_token = CharField()
    uid = CharField()
    email = CharField(null=True)

    class Meta:
        database = db

    def as_tuple(self):
        fields = ('service', 'access_token', 'uid', 'email')
        return tuple(getattr(self, n) for n in fields)


if not os.path.exists(filepath):
    Account.create_table()

def load_accounts():
    return Account.select().tuples()

def remove_account(service, uid):
    Account.get(Account.service==service, Account.uid==uid).delete_instance()