# (c) Nguyễn Hồng Quân <ng.hong.quan@gmail.com>

import os.path
import json
import mimetypes
import imghdr
from io import BytesIO
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email import encoders
from email.generator import BytesGenerator
from urllib.parse import urlsplit, parse_qs, urljoin
from rauth.service import OAuth2Service, OAuth2Session
from requests.exceptions import RequestException

from .env import HTTP_PORT

FACEBOOK_CLIENT_ID = '245061975637232'
FACEBOOK_CLIENT_SECRET = '6ad08677db7c9ab12032439979ce7838'

GOOGLE_CLIENT_ID = '438853721692.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'AvDEdUEROKSUsi-K70Oc71-a'



class BaseService(OAuth2Service):
    def get_auth_token(self, success_url):
        o = urlsplit(success_url)
        d = parse_qs(o.query)
        self.auth_token = d['code'][0]
        return self.auth_token

    def is_authorize_success_url(self, url):
        pass

    def get_access_token(self):
        pass

    def get_auth_session(self):
        pass



class FacebookService(BaseService):
    def __init__(self):
        self.app = OAuth2Service(
            client_id=FACEBOOK_CLIENT_ID,
            client_secret=FACEBOOK_CLIENT_SECRET,
            name='facebook',
            authorize_url='https://graph.facebook.com/oauth/authorize',
            access_token_url='https://graph.facebook.com/oauth/access_token',
            base_url='https://graph.facebook.com/')

        self.redirect_uri = 'https://www.facebook.com/connect/login_success.html'
        params = {'scope': 'email,user_photos,publish_stream,photo_upload',
                  'response_type': 'code',
                  'display': 'popup',
                  'redirect_uri': self.redirect_uri}
        self.authorize_url = self.app.get_authorize_url(**params)
        self.auth_token = None
        self.access_token = None


    def is_authorize_success_url(self, url):   # URL checker
        if url.startswith('https://www.facebook.com/connect/login_success.html?code='):
            self.get_auth_token(url)
            return True
        return False


    def get_access_token(self):
        params = {'code': self.auth_token,
                  'redirect_uri': self.redirect_uri,
                  'grant_type': 'client_credentials'}
        token = self.app.get_access_token(params=params)
        self.access_token = token
        return token


    def get_auth_session(self):
        data = {'code': self.auth_token,
                'redirect_uri': self.redirect_uri}
        return self.app.get_auth_session(data=data)



class GoogleService(BaseService):
    def __init__(self):
        self.app = OAuth2Service(
            client_id=GOOGLE_CLIENT_ID,
            client_secret=GOOGLE_CLIENT_SECRET,
            name='google',
            authorize_url='https://accounts.google.com/o/oauth2/auth',
            access_token_url='https://accounts.google.com/o/oauth2/token',
            base_url='https://www.googleapis.com/plus/v1/people/')

        self.redirect_uri = 'http://localhost:{}'.format(HTTP_PORT)
        # Get authorized token.
        # Ref: https://developers.google.com/accounts/docs/OAuth2InstalledApp#formingtheurl
        params = {'scope': 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://picasaweb.google.com/data/',
                  'response_type': 'code',
                  'state': '',
                  'login_hint': 'email address',
                  'redirect_uri': self.redirect_uri}
        self.authorize_url = self.app.get_authorize_url(**params)
        self.auth_token = None
        self.access_token = None


    def is_authorize_success_url(self, url):   # URL checker
        if url.startswith(self.redirect_uri):
            self.get_auth_token(url)
            return True
        return False


    def get_access_token(self):
        # Ref: https://developers.google.com/accounts/docs/OAuth2InstalledApp#handlingtheresponse
        params = {'code': self.auth_token,
                  'client_id': self.app.client_id,
                  'client_secret': self.app.client_secret,
                  'redirect_uri': self.redirect_uri,
                  'grant_type': 'authorization_code'}
        print('PARAMS', params)
        token = self.app.get_access_token(params=params,
                                          decoder=decode_google_access_token)
        self.access_token = token
        return token


    def get_auth_session(self):
        data = {'code': self.auth_token,
                'client_id': self.app.client_id,
                'client_secret': self.app.client_secret,
                'redirect_uri': self.redirect_uri,
                'grant_type': 'authorization_code'}
        return self.app.get_auth_session(data=data, decoder=decode_google_access_token)



class BaseSession(OAuth2Session):
    def request(self, method, url, *args, **kwargs):
        if not url.startswith('https://') and not url.startswith('http://'):
            url = urljoin(self.base_url, url)
        return super().request(method, url, *args, **kwargs)



class FacebookSession(BaseSession):
    def __init__(self, access_token):
        super().__init__(FACEBOOK_CLIENT_ID, FACEBOOK_CLIENT_SECRET, access_token)
        self.base_url='https://graph.facebook.com/'


    def upload_photo(self, album, filepath, desc=''):
        url = '{}/photos'.format(album)
        files = {'source': open(filepath, 'rb')}
        data = {'message': desc}
        r = self.post(url, files=files, data=data)
        jres = r.json()
        try:
            return int(jres['id'])
        except Exception:
            return False


    def add_album(self, name):
        data = {
            'name': name,
            'message': ''
        }
        r = self.post('me/albums', data=data)
        jres = r.json()
        try:
            return int(jres['id'])
        except KeyError:
            return False


    def get_albums(self):
        data = []
        timeline_album = self.get_timeline_album()
        if timeline_album:
            data.append(timeline_album)
        resp = self.get('me/albums', params={'fields': 'id,name,count,can_upload'})
        if not resp.ok:
            yield data.get(0)
            return
        result = resp.json()
        data.extend(a for a in result['data'] if a['can_upload'])
        while 'next' in result.get('paging', ()):
            url = result['paging']['next']
            resp = self.get(url)
            if not resp.ok:
                break
            result = resp.json()
            data.extend(a for a in result['data'] if a['can_upload'])

        for alb in data:
            yield alb['id'], alb['name'], alb.get('count', 0)


    def get_timeline_album(self):
        ''' Create album info with user's ID as album's ID '''
        resp = self.get('me/albums',
                        params={'fields': 'type,count,name,from', 'limit': 4})
        if not resp.ok:
            raise RequestException('Getting info via Facebook Graph API failed')
        result = resp.json()
        for a in result['data']:
            if a['type'] == 'wall':
                return {
                    'id': a['from']['id'],
                    'name': a['name'],
                    'count': a.get('count', 0)
                }
        return None



class GoogleSession(BaseSession):
    def __init__(self, access_token, userid=None):
        super().__init__(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, access_token)
        self.base_url='https://picasaweb.google.com/data/feed/api/'
        self.userid = userid


    def request(self, method, url, *args, **req_kwargs):
        ''' Override OAuth2Session's request(), to add 'GData-Version'
        field to header, and let Google know that we want JSON response.
        Ref: https://developers.google.com/picasa-web/docs/2.0/developers_guide_protocol#Versioning
        '''
        # Specify Google Data version
        req_kwargs.setdefault('headers', {})
        req_kwargs['headers'].update({'GData-Version': 2})
        # Use JSON as response format
        req_kwargs.setdefault('params', {})
        req_kwargs['params'].update({'alt': 'json'})
        return super().request(method, url, *args, **req_kwargs)


    def add_album(self, name):
        atom = '''<entry xmlns='http://www.w3.org/2005/Atom'
        xmlns:media='http://search.yahoo.com/mrss/'
        xmlns:gphoto='http://schemas.google.com/photos/2007'>
        <title type='text'>{name}</title>
        <gphoto:access>public</gphoto:access>
        <media:group>
        </media:group>
        <category scheme='http://schemas.google.com/g/2005#kind'
          term='http://schemas.google.com/photos/2007#album'></category>
        </entry>'''.format(name=name)
        headers = {
            'Content-Type': 'application/atom+xml',
        }
        r = self.post('user/default', data=atom, headers=headers)
        if r.status_code != 201:
            return False
        try:
            return int(r.json()['entry']['gphoto$id']['$t'])
        except KeyError:
            return False


    def delete_album(self, aid):
        url = 'https://picasaweb.google.com/data/entry/api/user/default/albumid/{}'.format(aid)
        r = self.request('DELETE', url, headers={'If-Match': '*'})
        return r.ok


    def get_albums(self):
        # Ref: https://developers.google.com/picasa-web/docs/2.0/developers_guide_protocol#ListAlbums
        resp = self.get('user/default',
                        params={'fields': 'entry(title,gphoto:id,gphoto:numphotos)'})
        if not resp.ok:
            yield []
            return
        try:
            entries = resp.json()['feed']['entry']
        except KeyError:
            yield []
            return
        for alb in entries:
            name = alb['title']['$t']
            count = int(alb['gphoto$numphotos']['$t'])
            aid = alb['gphoto$id']['$t']
            yield (aid, name, count)


    def upload_photo(self, album, filepath, desc=''):
        ''' Due to failure in uploading photo by "multipart/related" method,
        I do in two steps: First, upload raw photo without metadata, then
        update the metadata '''
        pid, edit_link = self.upload_photo_file_only(album, filepath)
        if pid and edit_link:
            self.update_metadata(edit_link, filepath, desc)
        return int(pid) if pid else False


    def upload_photo_file_only(self, album, filepath):
        ''' Return photo ID and edit link'''
        url = 'user/default/albumid/{}'.format(album)
        mtype, encoding = mimetypes.guess_type(filepath)
        if not mtype:  # Cannot guess via filename
            imgtype = imghdr.what(filepath)
            if not imgtype:
                # Cannot determine image type, stop now.
                return False, 'File is not an image'
            mtype = 'image/' + imgtype
        headers = {
            'Content-Type': mtype
        }
        with open(filepath, 'rb') as fl:
            r = self.post(url, data=fl, headers=headers)
        if r.status_code != 201 or not r.ok: # Upload failed
            return False, r.content
        # Upload successfully
        jresp = r.json()
        pid = jresp['entry']['gphoto$id']['$t']
        for l in jresp['entry']['link']:
            if l['rel'] == 'edit':
                edit_link = l['href']
                return pid, edit_link
        return pid, None


    def update_metadata(self, edit_link, filepath, desc):
        filename = os.path.basename(filepath)
        headers = {
            'Content-Type': 'application/xml',
            'If-Match': '*'
        }
        body = '''<entry xmlns='http://www.w3.org/2005/Atom'>
        <title>{title}</title>
        <summary>{summary}</summary>
        <category scheme="http://schemas.google.com/g/2005#kind"
          term="http://schemas.google.com/photos/2007#photo"/>
        </entry>'''.format(title=filename, summary=desc).encode('utf-8')
        r = self.request('PATCH', edit_link,
                         data=body, headers=headers)
        return r.ok



#--- Functions ---#

def decode_google_access_token(s):
    # 's' received is of bytes type
    return json.loads(s.decode())


def build_picasa_post_content(filepath, desc):
    # This function is supposed to build content for posting
    # photo in recommended way. But I failed to do it.
    filename = os.path.basename(filepath)
    related = MIMEMultipart('related', 'END_OF_PART')
    templ = '''<entry xmlns='http://www.w3.org/2005/Atom'>
    <title>{title}</title>
    <summary>{summary}</summary>
    <category scheme="http://schemas.google.com/g/2005#kind"
      term="http://schemas.google.com/photos/2007#photo"/>
    </entry>'''
    xml = templ.format(title=filename, summary=desc)
    # Ref: http://stackoverflow.com/questions/15746558/how-to-send-a-multipart-related-with-requests-in-python
    metadata = MIMEApplication(xml, 'atom+xml', encoders.encode_noop)
    related.attach(metadata)
    with open(filepath, 'rb') as fl:
        image = MIMEImage(fl.read(), _encoder=encoders.encode_noop)
    related.attach(image)
    headers = dict(related.items())
    headers['Slug'] = filename
    fp = BytesIO()
    g = BytesGenerator(fp)
    g.flatten(related)
    body = fp.getvalue().split(b'\n\n', 1)[1]
    body = b'Media multipart posting\n' + body.replace(b'MIME-Version: 1.0\n', b'')
    headers['Content-Length'] = len(body)
    return headers, body
