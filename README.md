QPhotoUpload
=============

QPhotoUpload is an application to help upload photos to social networks such as Facebook, Google Picasa...

QPhotoUpload is written in Python3 and GTK3 (through GObject-Introspection), with no dependence on GNOME, to be able to run in XFCE, LXDE and KDE.

Despite of first letter Q, it is not a Qt app. "Q" is just my name, Quân (hehe).

## Features

1. Upload photos, include description, to Facebook, Google (Picasa/Plus)

1. Resize photo if wanted before uploading to save bandwidth.

1. Support drag and drop.

![Alt QPhotoUpload](http://i.imgur.com/Py3EosY.png)

## Install

QPhotoUpload has a PPA repository for installing on Ubuntu at

```
ppa:ng-hong-quan/ppa
```
(just available for _Raring Ringtail_ only)

## Interface for other applications

Other applications, no matter what type of GUI toolkit, can call QPhotoUpload and send photos to it, by executing

```
qphotoupload -
```

(note the dash at the end) then pass a lists of file paths via `stdin`, each in a line.

1. Example with shell:

```
echo -e 'photo1.png\nphoto2.jpg' | qphotoupload -
```

1. Example with Python

```python
import subprocess
files = ('/home/user/photo1.png', '/home/user/photo2.jpg')
p = subprocess.Popen(['qphotoupload', '-'], stdin=subprocess.PIPE)
p.stdin.write('\n'.join(files))
p.stdin.close()
```

## Why I create QPhotoUpload

1. I want to see my photos, select and upload right away, without having to open browser.

2. In the past, I used gThumb to upload my photo to Facebook, but the feature has been broken recently. I can't wait for the author to fix it. I also cannot fix on my own, because looking to C code is a pain. You may ask why not use Shotwell. Well, I don't like its style of "import". I have my photos organized already in folders. I don't want to import again to somewhere else. Moreover, I want to select photos right from Nautilus, instead of opening more application and browse for my photos again.

## Credit
- Icon: DarKobra from DeviantArt
