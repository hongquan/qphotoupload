#!/usr/bin/env python3

import sys, os, shutil, glob
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

sys.path.insert(1, 'src')
from qphotoupload.env import __version__, PACKAGE_NAME

# Clean pycache
cachefolder = os.path.join('src', PACKAGE_NAME, '__pycache__')
shutil.rmtree(cachefolder)

# Copy the script to other place, rename it for building
src_bin = os.path.join('src', PACKAGE_NAME+'-bin')
bin_folder = 'bin'
bin_file = os.path.join(bin_folder, PACKAGE_NAME)

if len(sys.argv) > 1:
    if not os.path.isdir(bin_folder):
        os.mkdir(bin_folder)
    print('Copy {} to {}'.format(src_bin, bin_file))
    shutil.copy(src_bin, bin_file)


def get_data_files():
    r = glob.glob('data/*')
    r.extend(glob.glob('data/icons/*'))
    r.remove('data/icons')
    return r


setup(name=PACKAGE_NAME,
    version=__version__,
    description='QPhotoUpload is an application to help upload photos to social networks such as Facebook, Google Picasa....',
    author='Nguyễn Hồng Quân',
    author_email='ng.hong.quan@gmail.com',
    url='http://heomoi.wordpress.com',
    package_dir = {'': 'src'},
    packages=[PACKAGE_NAME],
    scripts=[bin_file],
    data_files=[('share/'+PACKAGE_NAME, glob.iglob('data/*.*')),
                ('share/{}/icons'.format(PACKAGE_NAME), glob.iglob('data/icons/*.*')),
                ('share/icons/hicolor/scalable/apps',
                 ['data/icons/{}.svg'.format(PACKAGE_NAME)]),
                ('share/applications', ['data/{}.desktop'.format(PACKAGE_NAME)])],
    install_requires=[
        'requests >= 1.0',
        'rauth >= 0.5',
        'feedparser',
        'peewee',
    ]
)

# Remove the copied file
if os.path.exists(bin_folder):
    print('Remove', bin_folder)
    shutil.rmtree(bin_folder)
